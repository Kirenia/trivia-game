
import ResultComponent from './components/ResultComponent.vue'
import App from './App.vue'

const routes = [
    // { path: '/', component: App },
    { path: '/answer/:corrects', component: ResultComponent }
];

export default routes;